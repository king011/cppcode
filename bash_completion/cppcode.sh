#!/bin/bash
#Program:
#       cppcode for bash completion
#History:
#       2018-09-14 king011 first release
#Email:
#       zuiwuchang@gmail.com
function _king011_cppcode_basic_()
{
    COMPREPLY=($(compgen -W 'new generate help --help -h -v' -- "$word"))
}
function _king011_cppcode_new_()
{
    COMPREPLY=($(compgen -W '--cli -c --gui -g --mingw --help -h' -- "$word"))
}
function _king011_cppcode_generate_()
{
    case ${COMP_WORDS[COMP_CWORD-1]} in
        '-t')
            COMPREPLY=()
        ;;
        '--target')
            COMPREPLY=()
        ;;

        '-m')
            COMPREPLY=($(compgen -W 'class wx header' -- "$word"))
        ;;
        '--mode')
            COMPREPLY=($(compgen -W 'class wx header' -- "$word"))
        ;;

        # default
        *)
            COMPREPLY=($(compgen -W '--target -t --mode -m --help -h' -- "$word"))
        ;;
    esac
}
function _king011_cppcode_()
{
    # 獲取 正在輸入的 參數
    local word=${COMP_WORDS[COMP_CWORD]}

    # switch 輸入第 幾個參數
    if [ 1 == $COMP_CWORD ];then
        _king011_cppcode_basic_
    else
        # ng 子命令
        case ${COMP_WORDS[1]} in
            'new')
                _king011_cppcode_new_
            ;;

            'generate')
                _king011_cppcode_generate_
            ;;
        
            *)	# default
                COMPREPLY=()
            ;;
        esac
        
    fi
}

complete -F _king011_cppcode_ cppcode