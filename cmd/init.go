package cmd

import (
	"fmt"
	"github.com/spf13/cobra"
	"gitlab.com/king011/cppcode/version"
	"os"
)

const (
	// App 程式名
	App = "cppcode"
)

var v bool
var rootCmd = &cobra.Command{
	Use:   App,
	Short: "tools for vscode+cmake+gcc",
	Run: func(cmd *cobra.Command, args []string) {
		if v {
			fmt.Println(version.Tag)
			fmt.Println(version.Commit)
			fmt.Println(version.Date)
		} else {
			fmt.Println(App)
			fmt.Println(version.Tag)
			fmt.Println(version.Commit)
			fmt.Println(version.Date)
			fmt.Printf(`Use "%v --help" for more information about this program.
`, App)
		}
	},
}

func init() {
	flags := rootCmd.Flags()
	flags.BoolVarP(&v,
		"version",
		"v",
		false,
		"show version",
	)
}

// Execute 執行命令
func Execute() error {
	return rootCmd.Execute()
}
func abort() {
	os.Exit(1)
}
