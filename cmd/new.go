package cmd

import (
	"github.com/spf13/cobra"
	"gitlab.com/king011/cppcode/code"
	"log"
	"strings"
)

func init() {
	var cli, gui, mingw string
	cmd := &cobra.Command{
		Use:   "new",
		Short: "create new project",
		Long: `create new project
	cppcode new -g project-name
	cppcode new -c project-name
`,
		Run: func(cmd *cobra.Command, args []string) {
			cli = strings.TrimSpace(cli)
			gui = strings.TrimSpace(gui)
			if cli == "" && gui == "" {
				cmd.Help()
				abort()
			}
			context, e := code.New()
			if e != nil {
				log.Fatalln(e)
			}
			if cli != "" {
				context.NewCLI(&code.DataNew{
					Target: cli,
					Mingw:  mingw,
				})
			}
			if gui != "" && cli != gui {
				context.NewGUI(&code.DataNew{
					Target: gui,
					Mingw:  mingw,
				})
			}
		},
	}
	flasg := cmd.Flags()
	flasg.StringVarP(&cli, "cli", "c", "", "create a new cli project")
	flasg.StringVarP(&gui, "gui", "g", "", "create a new gui project")
	flasg.StringVar(&mingw, "mingw", "f:/msys64/mingw32", "mingw path only use in windows")
	rootCmd.AddCommand(cmd)
}
