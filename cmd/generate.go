package cmd

import (
	"github.com/spf13/cobra"
	"gitlab.com/king011/cppcode/code"
	"log"
)

func init() {
	var target, mode string
	cmd := &cobra.Command{
		Use:   "generate",
		Short: "generate a component",
		Long: `generate a component
	cppcode generate -t animal/dark-cat
	cppcode generate -t animal/dark-cat -m wx
	cppcode generate -t animal/dark-cat -m header
`,
		Run: func(cmd *cobra.Command, args []string) {
			context, e := code.New()
			if e != nil {
				log.Fatalln(e)
			}

			data, e := code.NewDataGenerate(target, mode)
			if e != nil {
				log.Fatalln(e)
			}
			context.Generate(data)
		},
	}
	flasg := cmd.Flags()
	flasg.StringVarP(&target, "target", "t", "", "target name")
	flasg.StringVarP(&mode, "mode", "m", "class", "generate class or wx-class or header")
	rootCmd.AddCommand(cmd)
}
