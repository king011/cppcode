#include "{{.Target}}App.h"
#include "{{.Target}}Main.h"

IMPLEMENT_APP({{.Target}}App);

bool {{.Target}}App::OnInit()
{
    {{.Target}}Main* frame = new {{.Target}}Main(0L, _("wxWidgets Application {{.Target}}"));
#ifdef WIN32
    frame->SetIcon(wxICON(aaaa)); // To Set App Icon
#endif
    frame->Show();
    
    return true;
}
