#ifndef __TEMPLATE_APP_{{.Target}}_H__
#define __TEMPLATE_APP_{{.Target}}_H__

#include <wx/app.h>

class {{.Target}}App : public wxApp
{
    public:
        virtual bool OnInit();
};

#endif // __TEMPLATE_APP_{{.Target}}_H__
