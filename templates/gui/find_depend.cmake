# Boost
find_package(Boost
    1.58.0
    REQUIRED
)
list(APPEND target_headers ${Boost_INCLUDE_DIRS})
list(APPEND target_libs ${Boost_LIBRARIES})

# wxWidgets
if(UNIX AND NOT MINGW)
    add_definitions(-D__WXGTK__)
endif()
find_package(wxWidgets 
    3.0
    REQUIRED
)
list(APPEND target_headers ${wxWidgets_INCLUDE_DIRS})
list(APPEND target_libs ${wxWidgets_LIBRARIES})

# 刪除 重複項
if(target_headers)
    list(REMOVE_DUPLICATES target_headers)
endif()
if(target_libs)
    list(REMOVE_DUPLICATES target_libs)
endif()
