#ifndef __TEMPLATE_MAIN_{{.Target}}_H__
#define __TEMPLATE_MAIN_{{.Target}}_H__

#include <wx/wx.h>

#include "{{.Target}}App.h"

class {{.Target}}Main: public wxFrame
{
    public:
        {{.Target}}Main(wxFrame *frame, const wxString& title);
        ~{{.Target}}Main();
    private:
        enum
        {
            idMenuQuit = 1000,
            idMenuAbout
        };
        void OnClose(wxCloseEvent& event);
        void OnQuit(wxCommandEvent& event);
        void OnAbout(wxCommandEvent& event);
        DECLARE_EVENT_TABLE()
};


#endif // __TEMPLATE_MAIN_{{.Target}}_H__