#include "{{.Target}}Main.h"

//helper functions
enum wxbuildinfoformat {
    short_f, long_f };

wxString wxbuildinfo(wxbuildinfoformat format)
{
    wxString wxbuild(wxVERSION_STRING);

    if (format == long_f )
    {
#if defined(__WXMSW__)
        wxbuild << _T("-Windows");
#elif defined(__WXMAC__)
        wxbuild << _T("-Mac");
#elif defined(__UNIX__)
        wxbuild << _T("-Linux");
#endif

#if wxUSE_UNICODE
        wxbuild << _T("-Unicode build");
#else
        wxbuild << _T("-ANSI build");
#endif // wxUSE_UNICODE
    }
    
#if _DEBUG
     wxbuild << _T(" debug");
#else
    wxbuild << _T(" release");
#endif // wxUSE_UNICODE
    return wxbuild;
}

BEGIN_EVENT_TABLE({{.Target}}Main, wxFrame)
    EVT_CLOSE({{.Target}}Main::OnClose)
    EVT_MENU(idMenuQuit, {{.Target}}Main::OnQuit)
    EVT_MENU(idMenuAbout, {{.Target}}Main::OnAbout)
END_EVENT_TABLE()

{{.Target}}Main::{{.Target}}Main(wxFrame *frame, const wxString& title)
    : wxFrame(frame, -1, title)
{
#if wxUSE_MENUS
    // create a menu bar
    wxMenuBar* mbar = new wxMenuBar();
    wxMenu* fileMenu = new wxMenu(_T(""));
    fileMenu->Append(idMenuQuit, _("&Quit\tAlt-F4"), _("Quit the application"));
    mbar->Append(fileMenu, _("&File"));

    wxMenu* helpMenu = new wxMenu(_T(""));
    helpMenu->Append(idMenuAbout, _("&About\tF1"), _("Show info about this application"));
    mbar->Append(helpMenu, _("&Help"));

    SetMenuBar(mbar);
#endif // wxUSE_MENUS

#if wxUSE_STATUSBAR
    // create a status bar with some information about the used wxWidgets version
    CreateStatusBar(2);
    SetStatusText(_("Hello Code::Blocks user!"),0);
    SetStatusText(wxbuildinfo(short_f), 1);
#endif // wxUSE_STATUSBAR

}


{{.Target}}Main::~{{.Target}}Main()
{
}

void {{.Target}}Main::OnClose(wxCloseEvent &event)
{
    Destroy();
}

void {{.Target}}Main::OnQuit(wxCommandEvent &event)
{
    Destroy();
}

void {{.Target}}Main::OnAbout(wxCommandEvent &event)
{
    wxString msg = wxbuildinfo(long_f);
    wxMessageBox(msg, _("Welcome to..."));
}
