#!/bin/bash

NewFile(){
	echo "$2" > "$1"
	if [ "$?" != 0 ] ;then
		exit 1
	fi
}
WriteFile(){
	echo "$2" >> "$1"
	if [ "$?" != 0 ] ;then
		exit 1
	fi
}

# 返回 git 信息 時間
tag=`git describe`
if [ "$tag" == '' ];then
    tag="[unknown tag]"
fi

commit=`git rev-parse HEAD`
if [ "$commit" == '' ];then
    commit="[unknow commit]"
fi

date=`date +'%Y-%m-%d %H:%M:%S'`

# 打印 信息
echo ${tag} $commit
echo $date

# 創建 .h
filename="version.h"
NewFile $filename	'#if !defined(__VERSION_H__)'
WriteFile $filename	'#define __VERSION_H__'
WriteFile $filename
WriteFile $filename "#define TARGET_GIT_TAG \"${tag}\""
WriteFile $filename "#define TARGET_GIT_COMMIT \"${commit}\""
WriteFile $filename "#define TARGET_GIT_DATE \"${date}\""
WriteFile $filename
WriteFile $filename	'#endif // __VERSION_H__'
