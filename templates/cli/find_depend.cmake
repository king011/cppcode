# Boost
find_package(Boost
    1.58.0
    REQUIRED
    COMPONENTS 
        date_time
)
list(APPEND target_headers ${Boost_INCLUDE_DIRS})
list(APPEND target_libs ${Boost_LIBRARIES})

# 刪除 重複項
if(target_headers)
    list(REMOVE_DUPLICATES target_headers)
endif()
if(target_libs)
    list(REMOVE_DUPLICATES target_libs)
endif()
