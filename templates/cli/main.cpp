#include <iostream>

#include <boost/date_time/posix_time/posix_time.hpp>

int main(int argc, char *argv[])
{
    std::cout << boost::posix_time::second_clock::local_time() << "\n";
#if defined(_DEBUG)
    std::cout << "this is {{.Target}} debug" << std::endl;
#else
    std::cout << "this is {{.Target}} release" << std::endl;
#endif // _DEBUG

    return 0;
}