#ifndef __{{.Header}}_H__
#define __{{.Header}}_H__

#include <wx/frame.h>

class {{.Target}}: public wxFrame
{
    public:
        {{.Target}}(wxFrame *frame, const wxString& title);
        ~{{.Target}}();
    private:

        DECLARE_EVENT_TABLE()
};
#endif // __{{.Header}}_H__