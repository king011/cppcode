#ifndef __{{.Header}}_H__
#define __{{.Header}}_H__
class {{.Target}}
{
    public:
    {{.Target}}();
    ~{{.Target}}();
};
#endif // __{{.Header}}_H__