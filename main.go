package main

import (
	"gitlab.com/king011/cppcode/cmd"
	"log"
)

func main() {
	if e := cmd.Execute(); e != nil {
		log.Fatalln(e)
	}
}
