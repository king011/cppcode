package code

import (
	"errors"
	"fmt"
	"gitlab.com/king011/king-go/os/fileperm"
	"io/ioutil"
	"log"
	"os"
	"runtime"
	"strings"
	"text/template"
)

func init() {
	log.SetFlags(log.LstdFlags | log.Lshortfile)
}

// Context .
type Context struct {
	paths map[string]bool
}

var errUnknowGOPATH = errors.New("unknow GOPATH")

// New 創建 一個 環境
func New() (context *Context, e error) {
	gopath := os.Getenv("GOPATH")
	var sep string
	if runtime.GOOS == "windows" {
		sep = ";"
	} else {
		sep = ":"
	}
	strs := strings.Split(gopath, sep)
	keys := make(map[string]bool)
	var ok bool
	for _, str := range strs {
		str = strings.TrimSpace(str)
		if strings.HasSuffix(str, "/") {
			str = str[:len(str)-1]
		} else if runtime.GOOS == "windows" && strings.HasSuffix(str, "\\") {
			str = str[:len(str)-1]
		}

		if str == "" {
			continue
		}
		if _, ok = keys[str]; ok {
			continue
		}
		keys[str] = true
	}
	if len(keys) == 0 {
		e = errUnknowGOPATH
		return
	}
	context = &Context{
		paths: keys,
	}
	return
}

// initDirectory 初始化 項目需要的 檔案夾
func (c *Context) initDirectory(data *DataNew) (e error) {
	e = os.MkdirAll(fmt.Sprintf("%s/.vscode", data.Target), fileperm.Directory)
	if e != nil {
		return
	}

	e = os.MkdirAll(fmt.Sprintf("%s/build/bin", data.Target), fileperm.Directory)
	if e != nil {
		return
	}
	e = os.MkdirAll(fmt.Sprintf("%s/build/debug", data.Target), fileperm.Directory)
	if e != nil {
		return
	}
	e = os.MkdirAll(fmt.Sprintf("%s/build/release", data.Target), fileperm.Directory)
	if e != nil {
		return
	}

	e = os.MkdirAll(fmt.Sprintf("%s/cmake/modules", data.Target), fileperm.Directory)
	if e != nil {
		return
	}
	return
}

// initBase 初始化 基本檔案
func (c *Context) initBase(data *DataNew) (e error) {
	// vscode
	if e = c.parseTemplate("templates/c_cpp_properties.json",
		fmt.Sprintf("%s/.vscode/c_cpp_properties.json", data.Target),
		data,
	); e != nil {
		return
	}
	if e = c.parseTemplate("templates/launch.json",
		fmt.Sprintf("%s/.vscode/launch.json", data.Target),
		data,
	); e != nil {
		return
	}
	if e = c.copyTemplate("templates/settings.json",
		fmt.Sprintf("%s/.vscode/settings.json", data.Target),
	); e != nil {
		return
	}
	if e = c.parseTemplate("templates/tasks.json",
		fmt.Sprintf("%s/.vscode/tasks.json", data.Target),
		data,
	); e != nil {
		return
	}

	// git
	if e = c.copyTemplate("templates/gitignore",
		fmt.Sprintf("%s/.gitignore", data.Target),
	); e != nil {
		return
	}
	if e = c.copyTemplatePerm("templates/version.sh",
		fmt.Sprintf("%s/version.sh", data.Target),
		fileperm.File|fileperm.AX,
	); e != nil {
		return
	}

	// cmake
	if e = c.parseTemplate("templates/CMakeLists.txt",
		fmt.Sprintf("%s/CMakeLists.txt", data.Target),
		data,
	); e != nil {
		return
	}
	if e = c.copyTemplate("templates/cotire.cmake",
		fmt.Sprintf("%s/cmake/cotire.cmake", data.Target),
	); e != nil {
		return
	}
	return
}

// NewCLI 創建一個 cli 項目
func (c *Context) NewCLI(data *DataNew) {
	ok, e := IsNotExist(data.Target)
	if e != nil {
		log.Fatalln(e)
	} else if !ok {
		log.Fatalf("%s already exists", data.Target)
	}

	defer func() {
		if e != nil {
			os.RemoveAll(data.Target)
			abort()
		}
	}()
	// 創建 檔案夾
	e = c.initDirectory(data)
	if e != nil {
		log.Println(e)
		return
	}

	// 初始化 共有 檔案
	e = c.initBase(data)
	if e != nil {
		return
	}
	// 初始化 專有 檔案
	// cmake
	if e = c.copyTemplate("templates/cli/find_depend.cmake",
		fmt.Sprintf("%s/cmake/find_depend.cmake", data.Target),
	); e != nil {
		return
	}
	if e = c.copyTemplate("templates/cli/source.cmake",
		fmt.Sprintf("%s/cmake/source.cmake", data.Target),
	); e != nil {
		return
	}
	// source
	if e = c.parseTemplate("templates/cli/main.cpp",
		fmt.Sprintf("%s/main.cpp", data.Target),
		data,
	); e != nil {
		return
	}

	log.Printf("new cli project : %s", data.Target)
}

// NewGUI 創建一個 gui 項目
func (c *Context) NewGUI(data *DataNew) {
	ok, e := IsNotExist(data.Target)
	if e != nil {
		log.Fatalln(e)
	} else if !ok {
		log.Fatalf("%s already exists", data.Target)
	}

	defer func() {
		if e != nil {
			os.RemoveAll(data.Target)
			abort()
		}
	}()
	// 創建 檔案夾
	e = c.initDirectory(data)
	if e != nil {
		log.Println(e)
		return
	}

	// 初始化 共有 檔案
	e = c.initBase(data)
	if e != nil {
		log.Println(e)
		return
	}
	// 初始化 專有 檔案
	// cmake
	if e = c.copyTemplate("templates/gui/find_depend.cmake",
		fmt.Sprintf("%s/cmake/find_depend.cmake", data.Target),
	); e != nil {
		return
	}
	if e = c.parseTemplate("templates/gui/source.cmake",
		fmt.Sprintf("%s/cmake/source.cmake", data.Target),
		data,
	); e != nil {
		return
	}

	// rc
	if e = c.parseTemplate("templates/gui/resource.rc",
		fmt.Sprintf("%s/resource.rc", data.Target),
		data,
	); e != nil {
		return
	}
	if e = c.parseTemplate("templates/gui/std.ico",
		fmt.Sprintf("%s/std.ico", data.Target),
		data,
	); e != nil {
		return
	}

	// source
	if e = c.parseTemplate("templates/gui/main.cpp",
		fmt.Sprintf("%s/%sMain.cpp", data.Target, data.Target),
		data,
	); e != nil {
		return
	}
	if e = c.parseTemplate("templates/gui/main.h",
		fmt.Sprintf("%s/%sMain.h", data.Target, data.Target),
		data,
	); e != nil {
		return
	}
	if e = c.parseTemplate("templates/gui/app.cpp",
		fmt.Sprintf("%s/%sApp.cpp", data.Target, data.Target),
		data,
	); e != nil {
		return
	}
	if e = c.parseTemplate("templates/gui/app.h",
		fmt.Sprintf("%s/%sApp.h", data.Target, data.Target),
		data,
	); e != nil {
		return
	}

	log.Printf("new gui project : %s", data.Target)
}
func (c *Context) copyTemplatePerm(src, target string, perm os.FileMode) (e error) {
	// 驗證 檔案 存在
	ok, e := IsNotExist(target)
	if e != nil {
		log.Println(e)
		return
	} else if !ok {
		e = fmt.Errorf("%s already exists", target)
		log.Println(e)
		return
	}

	// 查找 模板
	src, e = c.findFile(src)
	if e != nil {
		log.Println(e)
		return
	}

	// copy 模板
	var b []byte
	b, e = ioutil.ReadFile(src)
	if e != nil {
		log.Println(e)
		return
	}

	// 渲染模板
	e = ioutil.WriteFile(target, b, perm)
	return
}
func (c *Context) copyTemplate(src, target string) (e error) {
	return c.copyTemplatePerm(src, target, fileperm.File)
}
func (c *Context) parseTemplate(src, target string, data interface{}) (e error) {
	// 驗證 檔案 存在
	ok, e := IsNotExist(target)
	if e != nil {
		log.Println(e)
		return
	} else if !ok {
		e = fmt.Errorf("%s already exists", target)
		log.Println(e)
		return
	}

	// 查找 模板
	src, e = c.findFile(src)
	if e != nil {
		log.Println(e)
		return
	}

	// 創建模板
	var b []byte
	b, e = ioutil.ReadFile(src)
	if e != nil {
		log.Println(e)
		return
	}
	t := template.New(target)
	t, e = t.Parse(string(b))
	if e != nil {
		log.Println(e)
		return
	}

	// 渲染模板
	var f *os.File
	f, e = os.Create(target)
	if e != nil {
		log.Println(e)
		return
	}
	e = t.Execute(f, data)
	f.Close()
	if e != nil {
		log.Println(e)
		return
	}
	return
}
func (c *Context) findFile(src string) (abs string, e error) {
	var ok bool
	var filename string
	for dir := range c.paths {
		filename = dir + "/bin/cppcode/" + src
		ok, _ = IsExist(filename)
		if ok {
			abs = filename
			return
		}
		filename = dir + "/src/gitlab.com/king011/cppcode/" + src
		ok, _ = IsExist(filename)
		if ok {
			abs = filename
			return
		}
	}
	e = fmt.Errorf("file not found : %s", src)
	return
}

// Generate .
func (c *Context) Generate(data *DataGenerate) {
	switch data.Mode {
	case generateClass:
		c.generateClass(data)
	case generateWxClass:
		c.generateWxClass(data)
	case generateHeader:
		c.generateHeader(data)
	default:
		log.Fatalln("unknow generate mode : %v", data.Mode)
	}
}
func (c *Context) generateClass(data *DataGenerate) {
	data.MkdirAll()
	var e error
	if e = c.parseTemplate("templates/generate/class.h",
		data.GetAbs(data.Target+".h"),
		data,
	); e != nil {
		abort()
	}
	if e = c.parseTemplate("templates/generate/class.cpp",
		data.GetAbs(data.Target+".cpp"),
		data,
	); e != nil {
		abort()
	}
}
func (c *Context) generateWxClass(data *DataGenerate) {
	data.MkdirAll()
	var e error
	if e = c.parseTemplate("templates/generate/wx.h",
		data.GetAbs(data.Target+".h"),
		data,
	); e != nil {
		abort()
	}
	if e = c.parseTemplate("templates/generate/wx.cpp",
		data.GetAbs(data.Target+".cpp"),
		data,
	); e != nil {
		abort()
	}
}
func (c *Context) generateHeader(data *DataGenerate) {
	data.MkdirAll()
	var e error
	if e = c.parseTemplate("templates/generate/header.h",
		data.GetAbs(data.Target+".h"),
		data,
	); e != nil {
		abort()
	}
}
