package code

import (
	"os"
)

func abort() {
	os.Exit(1)
}

// IsExist 如果 檔案 已經存在 返回 true
func IsExist(name string) (ok bool, e error) {
	_, e = os.Stat(name)
	if e != nil {
		if os.IsNotExist(e) {
			e = nil
		}
		ok = false
		return
	}
	ok = true
	return
}

// IsNotExist 如果 檔案 不存在 返回 true
func IsNotExist(name string) (ok bool, e error) {
	_, e = os.Stat(name)
	if e != nil {
		if os.IsNotExist(e) {
			ok = true
			e = nil
			return
		}
		return
	}
	return
}
