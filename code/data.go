package code

import (
	"fmt"
	"gitlab.com/king011/king-go/os/fileperm"
	"os"
	"path"
	"path/filepath"
	"regexp"
	"runtime"
	"strings"
)

// DataNew .
type DataNew struct {
	Target string
	Mingw  string
}

const (
	generateClass = iota
	generateWxClass
	generateHeader
)

// DataGenerate .
type DataGenerate struct {
	// 項目 根目錄
	Root string
	// 目標名稱
	Target string
	// 檔案存儲 檔案夾
	Directory string
	// 創建類型
	Mode int
	// Header 宏
	Header string
}

// NewDataGenerate .
func NewDataGenerate(target, mode string) (data *DataGenerate, e error) {
	target = strings.TrimSpace(target)
	if target == "" {
		e = fmt.Errorf("target empty")
		return
	}
	mode = strings.TrimSpace(mode)

	var rs DataGenerate
	switch mode {
	case "class":
		rs.Mode = generateClass
	case "wx":
		rs.Mode = generateWxClass
	case "header":
		rs.Mode = generateHeader
	default:
		e = fmt.Errorf("unknow mode : %v", mode)
		return
	}

	rs.Directory = path.Dir(target)
	if rs.Directory == "." {
		rs.Directory = ""
	}
	base := path.Base(target[len(rs.Directory):])
	e = rs.SetTarget(base)
	if e != nil {
		return
	}
	e = rs.SetHeader(rs.Directory, rs.Target)
	if e != nil {
		return
	}

	e = rs.FindRoot()
	if e != nil {
		return
	}
	data = &rs
	return
}

var matchTarget = regexp.MustCompile(`([^a-zA-Z0-9\_\-])`)

// SetTarget .
func (d *DataGenerate) SetTarget(name string) (e error) {
	name = strings.TrimSpace(name)
	name = matchTarget.ReplaceAllString(name, "")
	strs := strings.Split(name, "-")

	target := ""
	last := ""
	for i, str := range strs {
		str = strings.TrimSpace(str)
		if str == "" {
			continue
		}
		if str[0] >= 'a' && str[0] <= 'z' {
			r := []rune(str)
			r[0] -= 32
			str = string(r)
		}

		if i == 0 {
			if str[0] >= '0' && str[0] <= '9' {
				target = "_" + str
			} else {
				target = str
			}

		} else {
			if str[0] >= '0' && str[0] <= '9' &&
				len(last) > 0 &&
				last[len(last)-1] >= '0' && last[len(last)-1] <= '9' {
				target += "_" + str
			} else {
				target += str
			}
		}
		last = str
	}

	if target == "" {
		e = fmt.Errorf("unknow target : %s", name)
	}
	d.Target = target
	return
}

// SetHeader .
func (d *DataGenerate) SetHeader(dir, target string) (e error) {
	dir = strings.Replace(dir, `\`, `_`, -1)
	dir = strings.Replace(dir, `/`, `_`, -1)
	dir = matchTarget.ReplaceAllString(dir, "")
	if !strings.HasSuffix(dir, "_") {
		d.Header = dir + "_" + target
	} else {
		d.Header = dir + target
	}
	d.Header = strings.ToUpper(d.Header)
	return
}

// FindRoot .
func (d *DataGenerate) FindRoot() (e error) {
	var dir string
	dir, e = filepath.Abs(".")
	if e != nil {
		return
	}
	var n int
	if runtime.GOOS == "windows" {
		n = 2
	} else {
		n = 1
	}
	var ok bool
	for len(dir) > n {
		ok, e = IsExist(dir + "/CMakeLists.txt")
		if e != nil {
			return
		} else if ok {
			d.Root = dir
			return
		}

		dir = path.Dir(dir)
	}
	e = fmt.Errorf("not found project")
	return
}

// GetAbs .
func (d *DataGenerate) GetAbs(file string) (abs string) {
	abs = d.Root
	if d.Directory != "" {
		if strings.HasSuffix(abs, `\`) || strings.HasSuffix(abs, `/`) {
			abs += d.Directory
		} else {
			abs += "/" + d.Directory
		}
	}
	if strings.HasSuffix(abs, `\`) || strings.HasSuffix(abs, `/`) {
		abs += file
	} else {
		abs += "/" + file
	}
	return
}

// MkdirAll .
func (d *DataGenerate) MkdirAll() {
	abs := d.Root
	if d.Directory != "" {
		if strings.HasSuffix(abs, `\`) || strings.HasSuffix(abs, `/`) {
			abs += d.Directory
		} else {
			abs += "/" + d.Directory
		}
	}

	os.MkdirAll(abs, fileperm.Directory)
}
